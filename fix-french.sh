#!/usr/bin/bash
# npx gulp CopyFrench
[ -d "src/routes/index-french" ] || exit

dest="src/routes/index-french"

while IFS= read -r -d '' file; do
  echo "$file"
  #dos2unix -q "$file"
  #npx prettier -w "$file"
  #remove trailing space
  sed -i 's/[[:space:]]*$//' "$file"
  #remove all blank lines
  # sed -i '/^$/d' "$file"
  # sed -i '/^\s$/d' "$file"
  # sed -i -e 's/^&nbsp;//g' "$file"

  # deletes leading blank lines
  sed -i '/./,$!d' "$file"

  if ! head -n 1 "$file" | grep -q "<script>"; then
    sed -i '1i<script>\n</script>\n' "$file"
  fi
  sed -i "2i import { StructuredList, StructuredListHead, StructuredListRow, StructuredListCell, StructuredListBody,Tile  } from 'carbon-components-svelte';" "$file"

  node ./dom-french.js "$file"
  sed -i '/^[[:space:]]*$/d' "$file"
  npx prettier -w "$file"

  sed -i '/^[[:space:]]*$/d' "$file"

done < <(find $dest -mindepth 2 -name "+page.svelte" -print0 | sort -z)
