import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';
if (process.argv.length !== 3) {
  console.error('Usage: node modify-dom.js path/to/filename.html');
  process.exit(1);
}
const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const partialHtml = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(partialHtml, { decodeEntities: false }, false);

$('html').remove(); // Removes <html> and its content
$('title').remove();
$('meta').remove();
$('link').remove();
$('script[src]').remove();
$('script[language]').remove();
$('style').remove();
$('br').remove();
$('center').remove();
$('[style]').removeAttr('style');
$('div.search1').remove(); // This should work as there's a div with id="search1"
$('div.altindex').remove(); // This is a class-based selector, not an ID
$('div.navig1').remove(); // The element uses a class, not an ID

// Replace <div class="title1"> with <h1 class="title1">
const $div = $('div.title1');
const innerContent = $div.html(); // Get the content inside the div
$div.replaceWith(`<h1 class="bx--type-expressive-heading-01">${innerContent}</h1>`);

const $haut = $('div#haut');
const hautContent = $haut.html(); // Get the content inside the div
$haut.replaceWith(`
  <Tile class="haut">
  <Grid>
  <Row>
  <Column>
      ${hautContent}
  </Column>
  </Row>
  </Grid>
  </Tile>
`);

// Select the single div#bas element
const $divbas = $('div#basfr');
const basContent = $divbas.html();

// Replace div#bas with the Tile structure
$divbas.replaceWith(`
<Tile id="basfr">
  <Grid>
  <Row>
  <Column>
  <StructuredList>
    <StructuredListBody>
      ${basContent}
    </StructuredListBody>
  </StructuredList>
  </Column>
  </Row>
  </Grid>
</Tile>
`);

const $TtlP = $('p.TtlP');
const columnHtml = `
  <Row>
  <Column>
  <p class="TtlP">${$TtlP.html()}</p>
  </Column>
  </Row>
`;

const $tileBas = $('Tile#basfr');
$tileBas.find('Grid').prepend(columnHtml);
$TtlP.remove();

$('a[href*=".htm"]').attr('href', (_, href) => href.replace('.htm', ''));

const $table = $('table');

// Add necessary classes to table elements
$table.addClass('bx--table');
$table.find('thead').addClass('bx--table-head');
$table.find('tr').addClass('bx--table-row');
$table.find('th').addClass('bx--table-header');
$table.find('td').addClass('bx--table-cell');

// Add <tbody> to tables without one
if ($table.find('tbody').length === 0) {
  const rows = $table.find('tr');
  if (rows.length > 0) {
    const $tbody = $('<tbody></tbody>');
    rows.each((_, row) => $tbody.append(row));
    $table.append($tbody);
  }
}

// fix hrefs to words starting with -  (remove the - in the page)
$('a[href*="lexicon/-"]').each((_, elem) => {
  const $elem = $(elem);
  const href = $elem.attr('href');
  const text = $elem.text();

  if (text.length > 1) {
    const replacementChar = text[1];
    const updatedHref = href.replace(/lexicon\/-/, `lexicon/${replacementChar}`);
    $elem.attr('href', updatedHref);
  }
});

$('span.IxFr').each((_, el) => {
  const $span = $(el);
  const $link = $span.find('a[name]'); // Find the <a> tag with a name attribute

  if ($link.length > 0) {
    $span.attr('id', $link.attr('name'));
    $link.replaceWith($link.contents()); // This replaces the <a> with its inner content (text)
  }
});

const modifiedHtml = $.html()
  .replace(/<tooltip>/g, '<Tooltip>')
  .replace(/<\/tooltip>/g, '</Tooltip>')
  .replace(/<tile /g, '<Tile ')
  .replace(/<\/tile/g, '</Tile')
  .replace(/<tag /g, '<Tag ')
  .replace(/<\/tag/g, '</Tag')
  .replace(/head=""/g, 'head')
  .replace(/<structuredlistcell/g, '<StructuredListCell')
  .replace(/<\/structuredlistcell>/g, '</StructuredListCell>')
  .replace(/<structuredlistbody>/g, '<StructuredListBody>')
  .replace(/<\/structuredlistbody>/g, '</StructuredListBody>')
  .replace(/<structuredlistrow/g, '<StructuredListRow')
  .replace(/<\/structuredlistrow>/g, '</StructuredListRow>')
  .replace(/<structuredlisthead/g, '<StructuredListHead')
  .replace(/<\/structuredlisthead>/g, '</StructuredListHead>')
  .replace(/<structuredlist/g, '<StructuredList')
  .replace(/<\/structuredlist>/g, '</StructuredList>');

fs.writeFileSync(absoluteFilePath, modifiedHtml);
