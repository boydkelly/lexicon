const autoprefixer = require('autoprefixer');
const postcssPresetEnv = require('postcss-preset-env');
const postcssImport = require('postcss-import');
const postcssGlobalData = require('@csstools/postcss-global-data');
const postcssJitProps = require('postcss-jit-props');
const OpenProps = require('open-props');

const config = {
  plugins: [
    postcssImport,
    postcssJitProps(OpenProps),
    postcssJitProps({
      files: ['src/assets/css/carbon-color.css']
    }),
    postcssGlobalData({
      files: ['src/assets/css/custom-media-queries.css']
    }),
    postcssPresetEnv({
      stage: 3,
      features: {
        'nesting-rules': true,
        'custom-media-queries': true,
        'media-query-ranges': true
      }
    }),
    autoprefixer()
  ]
};

module.exports = config;
