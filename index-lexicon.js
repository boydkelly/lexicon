import fs from 'fs';
import * as cheerio from 'cheerio';
import Fuse from 'fuse.js';
import path from 'path';

const dataFilePath = './src/lib/data-lex.json';
const indexFilePath = './src/lib/index-lex.json';
const jsonData = [];
const removeAccents = (str) => str.normalize('NFD').replace(/[\u0300-\u036F]/g, '');

function readHTMLFile(filePath, directoryName) {
  const htmlContent = fs.readFileSync(filePath, 'utf8');
  const $ = cheerio.load(htmlContent);

  const spansLxe = $('span.Lxe[id]');

  spansLxe.each((index, Lxe) => {
    const lxeId = directoryName + '#' + $(Lxe).attr('id'); // Prepend directory name to id
    const lxeText = $(Lxe).html();
    const lxeNoDiacritics = removeAccents(lxeText);
    if (lxeText.length > 0) {
      // Extract information from each span
      jsonData.push({
        lxeId,
        lxeNoDiacritics,
        lxeText
      });
    }
  });
}

// Root directory for lexicon files
const rootDirectory = 'src/routes/lexicon/';

// Traverse subdirectories under the root directory
const subdirectories = fs.readdirSync(rootDirectory).filter((subdir) => {
  const subdirPath = path.join(rootDirectory, subdir);
  return fs.statSync(subdirPath).isDirectory();
});

subdirectories.forEach((subdir) => {
  const subdirPath = path.join(rootDirectory, subdir);
  const htmlFilePath = path.join(subdirPath, '+page.svelte');

  if (fs.existsSync(htmlFilePath)) {
    // Process HTML file
    readHTMLFile(htmlFilePath, subdir);
  }
});

// Save jsonData to data.json
const jsonDataJson = JSON.stringify(jsonData);
fs.writeFileSync(dataFilePath, jsonDataJson);
console.log(`JSON data has been saved to ${dataFilePath}`);

// Create and save Fuse index
const options = { keys: ['lxeText', 'lxeId', 'lxeNoDiacritics'] };
const index = Fuse.createIndex(options.keys, jsonData);
const indexJson = JSON.stringify(index);
fs.writeFileSync(indexFilePath, indexJson);
console.log(`JSON index has been saved to ${indexFilePath}`);
