import gulp from 'gulp';
import shell from 'gulp-shell';
import rename from 'gulp-rename';
import fs from 'fs';
import path from 'path';

// Use dynamic import inside the function
async function deleteFiles() {
  const { deleteAsync } = await import('del'); // Correct import

  const indexFrenchPaths = [...'abcdefghijklmnopqrstuvwxyz'].map(
    (x) => `src/routes/index-french/${x}`
  );
  const lexiconPaths = [...'acdefghijklmnopqrstuwxyzɔɲɛŋ'].map((x) => `src/routes/lexicon/${x}`);

  return deleteAsync([...indexFrenchPaths, ...lexiconPaths], { force: true });
}

gulp.task('deleteFiles', deleteFiles);

gulp.task(
  'extract',
  shell.task([
    'git clean -fdX src/routes/lexicon src/routes/index-french',
    'unzip -o -qq Bambara.zip -d . ',
    'find . -type f -exec dos2unix -q {} +'
  ])
);

gulp.task('copyHome', function () {
  return gulp
    .src(['Bambara/lexicon/index.htm'])
    .pipe(rename('Lexicon.svelte')) // Renames the file to Home.svelte
    .pipe(gulp.dest('src/routes/lexicon')); // Copies to the destination folder
});

gulp.task('copyFrench', function () {
  return gulp
    .src('Bambara/index-french/*.htm')
    .pipe(
      rename((path) => {
        const fileName = path.basename; // e.g., "a"
        path.dirname = `${fileName}`; // Create directory named after the file
        path.basename = '+page'; // Rename the file to "+page"
        path.extname = '.svelte'; // Change the extension to ".svelte"
      })
    )
    .pipe(gulp.dest('src/routes/index-french')); // Copy to destination
});

gulp.task('copyLexicon', function () {
  return gulp
    .src('Bambara/lexicon/[a-zA-Zɲŋɔɛ].htm')
    .pipe(
      rename((path) => {
        const fileName = path.basename; // e.g., "a"
        path.dirname = `${fileName}`; // Create directory named after the file
        path.basename = '+page'; // Rename the file to "+page"
        path.extname = '.svelte'; // Change the extension to ".svelte"
      })
    )
    .pipe(gulp.dest('src/routes/lexicon')); // Copy to destination
});

gulp.task('copyPagejs', function (done) {
  const lexiconPath = 'src/routes/lexicon';

  // Get all subdirectories inside lexicon
  const subdirectories = fs
    .readdirSync(lexiconPath)
    .filter((dir) => fs.statSync(path.join(lexiconPath, dir)).isDirectory());

  // Copy +page.js into each subdirectory
  subdirectories.forEach((dir) => {
    gulp.src('src/lib/+page.js').pipe(gulp.dest(path.join(lexiconPath, dir)));
  });

  done();
});

// tasks
gulp.task('buildLexicon', shell.task(['./fix-lexicon.sh']));
gulp.task('buildHome', shell.task(['./fix-home.sh']));
gulp.task('buildFrench', shell.task(['./fix-french.sh']));

gulp.task('indexLexicon', shell.task(['node index-lexicon.js']));
gulp.task('indexFrench', shell.task(['node index-french.js']));

gulp.task('copy', gulp.parallel('copyLexicon', 'copyFrench', 'copyHome'));
gulp.task('build', gulp.parallel('buildLexicon', 'buildFrench', 'buildHome'));
gulp.task('index', gulp.parallel('indexLexicon', 'indexFrench'));

gulp.task(
  'testBuild',
  gulp.series('copyLexicon', 'copyHome', 'deleteFiles', 'buildHome', 'buildLexicon', 'index')
);
gulp.task('default', gulp.series('extract', 'copy', 'copyPagejs', 'build', 'index'));
