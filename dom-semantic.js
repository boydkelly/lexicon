import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';

if (process.argv.length !== 3) {
  console.error('Usage: node modify-dom.js path/to/filename.html');
  process.exit(1);
}

const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const partialHtml = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(partialHtml, { decodeEntities: false });

const dictionaryUrl = 'https://bamadaba.coastsystems.net'; // Updated dictionary source

const lxPElements = $('p.lxP');

lxPElements.each((index, elem) => {
  const structuredListRow = $('<StructuredListRow></StructuredListRow>');
  const article = $('<article itemscope itemtype="https://schema.org/DefinedTerm"></article>');
  structuredListRow.append(article);

  // Clone <p class="lxP"> and remove existing itemprop
  const headword = $(elem).clone();
  headword.removeAttr('itemprop');

  // Find the <span class="Lxe"> inside <p class="lxP">
  const lxeSpan = headword.find('span.Lxe');
  if (lxeSpan.length) {
    lxeSpan.attr('itemprop', 'name'); // Move itemprop to <span class="Lxe">
  }

  article.append(headword);

  // Capture siblings up to either <p class="lxP"> or <span class="Lxe">
  const siblings = $(elem).nextUntil('p.lxP');

  article.append(siblings.clone());

  // Add the <link> element inside <article>
  const linkElement = $(`<link itemprop="inDefinedTermSet" href="${dictionaryUrl}">`);
  article.prepend(linkElement);

  // Replace the original <p class="lxP"> with the new <article>
  $(elem).replaceWith(structuredListRow);

  // Remove the now-wrapped siblings from the DOM
  siblings.remove();
});

// Fix misplaced <a> elements
$('article').each((index, article) => {
  const nextArticle = $(article).next('article');

  if (nextArticle.length) {
    let trailingLinks = $(article).children('a[name]:not([class])');

    while (trailingLinks.length && trailingLinks.last().is($(article).children().last())) {
      const lastLink = trailingLinks.last();
      lastLink.remove();

      // Move <a> after <link>, not before
      const linkElement = nextArticle.children('link[itemprop="inDefinedTermSet"]');
      if (linkElement.length) {
        linkElement.after(lastLink);
      } else {
        nextArticle.prepend(lastLink);
      }

      trailingLinks = $(article).children('a[name]:not([class])');
    }
  }
});

const modifiedHtml = $.html().replace(/\n{3,}/g, '\n\n');
fs.writeFileSync(absoluteFilePath, modifiedHtml);

// console.log('All <p class="lxP"> blocks have been wrapped into <article> elements.');
// console.log('Added itemprop="name" to <p class="lxP">.');
// console.log('Added <link itemprop="inDefinedTermSet"> inside each <article>.');
// console.log('HTML has been modified and saved.');
