module.exports = {
  plugins: ["html", "@html-eslint"],
  overrides: [
    {
      files: ["*.htm", "*.html"],
      parser: "@html-eslint/parser",
      rules: {
        "@html-eslint/quotes": ["error", "double"],
        "@html-eslint/indent": ["error"],
        "@html-eslint/element-newline": ["error"],
        "@html-eslint/require-doctype": ["off"],
      }
    },
  ],
};
