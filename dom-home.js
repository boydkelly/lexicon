import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';

if (process.argv.length !== 3) {
  console.error('Usage: node modify-dom.js path/to/filename.html');
  process.exit(1);
}
const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const partialHtml = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(partialHtml, { decodeEntities: false }, false);

$('html').remove(); // Removes <html> and its content
$('title').remove();
$('meta').remove();
$('link').remove();
$('script').remove();
$('#haut').remove();
$('style').remove();
$('center').remove();
$('[style]').removeAttr('style');

$('div#bas').each((i, div) => {
  const $div = $(div);
  let innerContent = $div.html(); // Get the content inside the div
  // innerContent = innerContent.replace(/<div([^>]*)>/, '<Grid$1>').replace(/<\/div>/, '</Grid>');
  $div.replaceWith(`
<Tile id="bas">
    <Grid padding>
    ${innerContent}
    </Grid>
</Tile>
    `);
});

$('a[href*=".htm"]').attr('href', (_, href) => href.replace('.htm', ''));

const titleContent = $('div.title').html(); // Get the content inside the div
$('div.title').replaceWith(`<h1 class="bx--type-expressive-heading-04 title">${titleContent}</h1>`);

// Select all <div> elements with the class "Gl"
$('div.Gl').each((i, elem) => {
  const $elem = $(elem);
  const newTag = $('<Tag></Tag>');
  newTag.html($elem.html());

  Object.entries($elem.attr()).forEach(([key, value]) => {
    newTag.attr(key, value);
  });
  $elem.replaceWith(newTag);
});

$('h1').wrap('<Row></Row>');
// process.exit(0); // Exit with a success code

console.log($('h2').length); // Logs the number of <h2> elements found
// Iterate over all <h2> elements

// Loop 1: For each <h2>, replace it with a <Row> element containing the <h2> content
$('h2').each((i, h2) => {
  const $h2 = $(h2);
  const innerContent = $h2.html(); // Get the content inside the <h2>

  // Replace <h2> with a new <Row> containing the <h2> content
  $h2.replaceWith(`
<Row>
    <Column>
    <h2>
  ${innerContent}
    </Column>
    </h2>
</Row>
  `);
});

// Loop 2: Move the next <section> element inside its corresponding <Row>
$('Row').each((i, row) => {
  const $row = $(row);
  const $nextElement = $row.next(); // Get the next sibling element

  // Check if the next sibling is a <section>
  if ($nextElement.is('section')) {
    // Move the <section> inside the <Row>
    $row.append($nextElement);

    // Optionally, remove the <section> from its previous position, but cheerio's .append() moves it
  }
});

// Select the first <Row> and wrap its contents in a <Column>
const $firstRow = $('Row:first'); // Get the first <Row>
const innerContent = $firstRow.html(); // Get the content inside the <Row>

// Replace <Row> with <Row> containing <Column> that wraps the row's content
// <Column xlg={{ span: 2, offset: 1 }}>
$firstRow.replaceWith(`
<Row>
  <Column>
    ${innerContent}
  </Column>
</Row>
`);

$('table').each((i, el) => {
  const $table = $(el);

  // Create the StructuredList container
  const $structuredList = $('<StructuredList>\n</StructuredList>\n');

  // Convert <thead> into StructuredListHead
  const $thead = $table.find('thead');
  if ($thead.length) {
    const $structuredListHead = $('<StructuredListHead>\n</StructuredListHead>\n');
    $thead.find('tr').each((j, tr) => {
      const $row = $('<StructuredListRow head>\n</StructuredListRow>\n');
      $(tr)
        .find('th')
        .each((k, th) => {
          const $cell = $('<StructuredListCell head>\n</StructuredListCell>\n');
          $cell.html($(th).html()); // Preserve the content
          $row.append($cell);
        });
      $structuredListHead.append($row);
    });
    $structuredList.append($structuredListHead);
  } else {
    // Add empty StructuredListHead if <thead> is missing
    const $structuredListHead = $(
      '<StructuredListHead>\n<StructuredListRow head>\n<StructuredListCell head></StructuredListCell>\n</StructuredListRow>\n</StructuredListHead>\n'
    );
    $structuredList.append($structuredListHead);
  }

  // Convert <tbody> (or all <tr> if <tbody> is missing) into StructuredListBody
  const $structuredListBody = $('<StructuredListBody>\n</StructuredListBody>\n');
  $table.find('tbody tr, tr').each((j, tr) => {
    const $row = $('<StructuredListRow>\n</StructuredListRow>\n');
    $(tr)
      .find('td')
      .each((k, td) => {
        const $cell = $('<StructuredListCell>\n</StructuredListCell>\n');
        $cell.html($(td).html()); // Preserve the content
        $row.append($cell);
      });
    $structuredListBody.append($row);
  });
  $structuredList.append($structuredListBody);

  // Replace the original table with the new StructuredList component
  $table.replaceWith($structuredList);
});

$('a[name]').each(function () {
  const nameValue = $(this).attr('name');
  $(this).replaceWith(`<span id="${nameValue}"></span>`);
});

$('a[class="ve"]').each(function () {
  const nameValue = $(this).attr('class');
  $(this).replaceWith(`<span class="${nameValue}"></span>`);
});
// Remove <small>(lien)</small> elements
$('small').each((i, el) => {
  if ($(el).text().trim() === '(lien)') {
    $(el).remove();
  }
});

// Loop to replace <ul> with <unordered-list>
$('ul').each((i, ul) => {
  const $ul = $(ul);
  const innerContent = $ul.html(); // Get the content inside the <ul>

  // Replace <ul> with <unordered-list> and keep the content inside
  $ul.replaceWith(`
<unorderedlist>
  ${innerContent}
</unorderedlist>
  `);
});

// Loop to replace <li> with <listitem>
$('li').each((i, li) => {
  const $li = $(li);
  const innerContent = $li.html(); // Get the content inside the <li>

  // Replace <li> with <listitem> and keep the content inside
  $li.replaceWith(`
<listitem>
  ${innerContent}
</listitem>
  `);
});

// Manually adjust the casing of <Tooltip> tags to ensure they are preserved
const cleanedHtml = $.html()
  .replace(/<tooltip>/g, '<Tooltip>')
  .replace(/<\/tooltip>/g, '</Tooltip>')
  .replace(/<grid/g, '<Grid')
  .replace(/<\/grid>/g, '</Grid>')
  .replace(/<row>/g, '<Row>')
  .replace(/<\/row>/g, '</Row>')
  .replace(/<column/g, '<Column')
  .replace(/<\/column>/g, '</Column>')
  .replace(/<tile/g, '<Tile')
  .replace(/<\/tile/g, '</Tile')
  .replace(/<tag/g, '<Tag')
  .replace(/<\/tag/g, '</Tag')
  .replace(/head=""/g, 'head')
  .replace(/padding=""/g, 'padding')
  .replace(/<structuredlistcell/g, '<StructuredListCell')
  .replace(/<\/structuredlistcell>/g, '</StructuredListCell>')
  .replace(/<structuredlistbody>/g, '<StructuredListBody>')
  .replace(/<\/structuredlistbody>/g, '</StructuredListBody>')
  .replace(/<structuredlistrow/g, '<StructuredListRow')
  .replace(/<\/structuredlistrow>/g, '</StructuredListRow>')
  .replace(/<structuredlisthead/g, '<StructuredListHead')
  .replace(/<\/structuredlisthead>/g, '</StructuredListHead>')
  .replace(/<listitem/g, '<ListItem')
  .replace(/<\/listitem>/g, '</ListItem>')
  .replace(/<unorderedlist/g, '<UnorderedList')
  .replace(/<\/unorderedlist>/g, '</UnorderedList>')
  .replace(/<structuredlist/g, '<StructuredList')
  .replace(/<\/structuredlist>/g, '</StructuredList>');

// const cleanedHtml = '<script></script>\n' + modifiedHtml;

// Save the modified HTML to the original file
// const modifiedHtml = $.html();
fs.writeFileSync(absoluteFilePath, cleanedHtml);
