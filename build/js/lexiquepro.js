/* Go to lexical entry from Index page - and highlight it */
function go(a, b) {
  parent.location = '/lexicon/' + a.toLowerCase() + '#e' + b;
}
// function goct(lx, ps, gloss) {
//   //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
//   window.open(
//     'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29' +
//       lx +
//       '.%2A"+%26+lemma%3D"' +
//       lx +
//       '"+%26+tag%3D"' +
//       ps +
//       '"+%26+gloss%3D"' +
//       gloss +
//       '"]',
//     'Corpus tonal'
//   );
// }
// function gocnt(lx, ps, gloss) {
//   //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
//   window.open(
//     'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29' +
//       lx +
//       '.%2A"+%26+lemma%3D"' +
//       lx +
//       '"+%26+tag%3D"' +
//       ps +
//       '"+%26+gloss%3D"' +
//       gloss +
//       '"]',
//     'Corpus non tonal'
//   );
// }
/* case de recherche */
function ɲini() {
  var fɔlɔ, filanan, daɲɛ, ɲinihandle;
  ɲinihandle = document.getElementById('ɲini');
  daɲɛ = ɲinihandle.value;
  if (daɲɛ.charAt(0) == '-') fɔlɔ = daɲɛ.charAt(1);
  else fɔlɔ = daɲɛ.charAt(0);
  fɔlɔ = fɔlɔ.toLowerCase();
  filanan = daɲɛ.charAt(1);
  if (fɔlɔ + filanan == 'ny') fɔlɔ = 'ɲ';
  parent.location = '/lexicon/' + fɔlɔ + '#' + daɲɛ;
}
function ɲinitubabukan() {
  var fɔlɔ, filanan, daɲɛ, ɲinihandle;
  ɲinihandle = document.getElementById('ɲini');
  daɲɛ = ɲinihandle.value;
  fɔlɔ = daɲɛ.charAt(0).toLowerCase();
  if (fɔlɔ == 'â' || fɔlɔ == 'ä' || fɔlɔ == 'à') fɔlɔ = 'a';
  if (fɔlɔ == 'ê' || fɔlɔ == 'ë' || fɔlɔ == 'è' || fɔlɔ == 'é') fɔlɔ = 'e';
  if (fɔlɔ == 'î' || fɔlɔ == 'ï') fɔlɔ = 'i';
  if (fɔlɔ == 'ô' || fɔlɔ == 'ö') fɔlɔ = 'o';
  if (fɔlɔ == 'û' || fɔlɔ == 'ü' || fɔlɔ == 'ù') fɔlɔ = 'u';
  parent.location = '/index-french/' + fɔlɔ + '#' + daɲɛ;
}
function ɲinien() {
  var fɔlɔ, daɲɛ, ɲinihandle;
  ɲinihandle = document.getElementById('ɲini');
  daɲɛ = ɲinihandle.value;
  fɔlɔ = daɲɛ.charAt(0).toLowerCase();
  parent.location = '/index-english/' + fɔlɔ + '.htm#' + daɲɛ;
}
function ɲiniru() {
  var fɔlɔ, daɲɛ, ɲinihandle;
  ɲinihandle = document.getElementById('ɲini');
  daɲɛ = ɲinihandle.value;
  fɔlɔ = daɲɛ.charAt(0).toLowerCase();
  parent.location = '../index-russian/' + fɔlɔ + '.htm#' + daɲɛ;
}
function ɲininko() {
  var fɔlɔ, daɲɛ, ɲinihandle;
  ɲinihandle = document.getElementById('ɲini');
  daɲɛ = ɲinihandle.value;
  fɔlɔ = daɲɛ.charAt(0).toLowerCase();
  parent.location = '../index-nko/' + fɔlɔ + '.htm#' + daɲɛ;
}
// document.onkeydown.old = function (evt) {
//   let k1;
//   if (!evt) evt = window.event; // so that it works on both std browsers like Firefox, and others like IE Chrome...
//   k1 = evt.keyCode;
//   if (k1 == '13') {
//     ɲini();
//   }
// };
/* cacher-montrer */
function showhide() {
  let infohandle;
  infohandle = document.getElementById('info');
  if (infohandle.style.visibility == 'hidden') infohandle.style.visibility = 'visible';
  else infohandle.style.visibility = 'hidden';
}
function vinput(siginiden) {
  let v;
  v = document.getElementById('ɲini');
  v.value = v.value + siginiden;
  v.focus();
}
/* mettre la case de recherche en haut (surtout pour mobiles!*/
function enhaut(vthis) {
  vthis.style.top = 0;
  vthis.style.width = '60%';
  vthis.style.backgroundColor = 'lightgreen';
  vthis.style.fontSize = '18pt';
}

/* =========== incorpor ted from hi.js =========== */
/* Fragment Highlight version 0.1 */

//*** This JavaScript highlight code is copyright 2003 by David Dorward; http://dorward.me.uk
//*** Re-use or modification permitted provided the previous line is included and
//*** modifications are indicated

/*********** Start of JavaScript Library *********/

//*** This JavaScript library is copyright 2002 by Gavin Kistner and Refinery; www.refinery.com
//*** Re-use or modification permitted provided the previous line is included

//Adds a new class to an object, preserving existing classes
function AddClass(obj, cName) {
  KillClass(obj, cName);
  return (obj.className += (obj.className.length > 0 ? ' ' : '') + cName);
}

//Removes a particular class from an object, preserving other existing classes.
function KillClass(obj, cName) {
  return (obj.className = obj.className.replace(
    new RegExp('^' + cName + '\\b\\s*|\\s*\\b' + cName + '\\b', 'g'),
    ''
  ));
}

/*********** End of JavaScript Library ***********/

/* Fragment Highlight */

/* Indicates area that has been linked to if fragment identifiers have
 * been used. Especially useful in situations where a short fragment
 * is near the end of a page. */

var fragHLed = '';
var fragExclude = 'header';

let x;
Array.prototype.search = function (myVariable) {
  for (x in this) if (x == myVariable) return true;
  return false;
};

/* Highlight link target if the visitor arrives at the page with a # */

function fragHLload() {
  fragHL(location.hash.substring(1));
}

/* Highlight link target from an onclick event after unhighlighting the old one */

function fragHL(frag) {
  if (fragHLed.length > 0 && document.getElementById(fragHLed)) {
    KillClass(document.getElementById(fragHLed), 'fragment');
  }
  if (frag.length > 0 && document.getElementById(frag)) {
    fragHLed = frag;
    AddClass(document.getElementById(frag), 'fragment');
  }
}

/* Add onclick events to all <a> with hrefs that include a "#"  */

function fragHLlink() {
  if (document.getElementsByTagName) {
    var an = document.getElementsByTagName('a');
    let i, hashash, hashashpos;
    for (i = 0; i < an.length; i++) {
      hashash = an.item(i).getAttribute('href');
      if (hashash != null) {
        hashashpos = hashash.indexOf('#');
        if (hashashpos > 0) {
          var fragment = an
            .item(i)
            .getAttribute('href')
            .substring(an.item(i).getAttribute('href').indexOf('#') + 1);
          if (fragExclude.search(fragment)) {
            var evn = "fragHL('" + fragment + "')";
            var fun = new Function('e', evn);
            an.item(i).onclick = fun;
          }
        }
      }
    }
  }
}

function updateLinks() {
  var currentPath = window.location.pathname;
  var basePath = currentPath.split('/').slice(0, -1).join('/');
  currentPath = window.location.pathname;
  var parts = currentPath.split('/');
  basePath = parts[1] === 'lexicon' && parts.length > 2 ? '/lexicon' : '/' + parts[1];
  basePath = currentPath.split('/').slice(0, -1).join('/');
  parts = currentPath.split('/');
  basePath = '/' + parts[1];

  var tocLinks = document.querySelectorAll('#a-z a');
  tocLinks.forEach(function (link) {
    var letter = link.getAttribute('data-letter');
    console.log('letter: ' + letter);
    var newHref = '';

    // Check if the currentPath matches /anything/[a-z]/
    //if (currentPath.match(/(lexicon|index-french)\/[a-z]ɛɔɲŋ/)) {
    if (basePath.match(/(lexicon|index-french)/)) {
      //  if (basePath.match(/(lexicon|index-french)(\/|$)/)) {
      // if (currentPath.includes('lexicon') || currentPath.includes('index-french')) {

      // If on a letter's page in the specified directories, use "../" as the base to navigate to a new letter
      newHref = basePath + '/' + letter;
      console.log('matched with newHref: ' + newHref);
    } else {
      // If in other directories, use "./" as the base to navigate to a letter's page
      newHref = 'lexicon/' + letter;
      //newHref = letter;
      console.log('unmatched with newHref: ' + newHref);
    }
    link.setAttribute('href', newHref);
    console.log('link :' + link);
    console.log('currentpath: ' + currentPath);
    console.log('basepath: ' + basePath);
    console.log('newref:' + newHref);
  });
}

//disable all this for sveltkit
// Call the updateLinks function when the page loads
// window.addEventListener('load', updateLinks);

/* Init the script */
/* hi.js + setDisamb of initial index-go.js */
// window.onload = function(){
//   setDisamb();
//   /* from hi.js */
//   fragHLload();
//   fragHLlink();
// };
