import fs from 'fs';
import * as cheerio from 'cheerio';
import Fuse from 'fuse.js';
import path from 'path';

const dataFilePath = './src/lib/data-french.json';
const indexFilePath = './src/lib/index-french.json';
const jsonData = [];

function readHTMLFile(filePath, directoryName) {
  const htmlContent = fs.readFileSync(filePath, 'utf8');
  const $ = cheerio.load(htmlContent);

  const spansIxFr = $('span.IxFr');

  spansIxFr.each((index, outerSpan) => {
    const IxFrText = $(outerSpan).text();

    const IxFrId = directoryName + '#' + $(outerSpan).attr('id');

    if (IxFrText.trim().length > 0) {
      jsonData.push({
        IxFrId,
        IxFrText
      });
    }
  });
}

// Root directory for index-french files
const rootDirectory = 'src/routes/index-french/';

// Traverse subdirectories under the root directory
const subdirectories = fs.readdirSync(rootDirectory).filter((subdir) => {
  const subdirPath = path.join(rootDirectory, subdir);
  return fs.statSync(subdirPath).isDirectory();
});

subdirectories.forEach((subdir) => {
  const subdirPath = path.join(rootDirectory, subdir);
  const htmlFilePath = path.join(subdirPath, '+page.svelte');

  if (fs.existsSync(htmlFilePath)) {
    // Process HTML file
    readHTMLFile(htmlFilePath, subdir);
  }
});

// Save jsonData to data-french.json
const jsonDataJson = JSON.stringify(jsonData);
fs.writeFileSync(dataFilePath, jsonDataJson);
console.log(`JSON data has been saved to ${dataFilePath}`);

// Create and save Fuse index
const options = { keys: ['IxFrText', 'IxFrId'] };
const index = Fuse.createIndex(options.keys, jsonData);
const indexJson = JSON.stringify(index);
fs.writeFileSync(indexFilePath, indexJson);
console.log(`JSON index has been saved to ${indexFilePath}`);
