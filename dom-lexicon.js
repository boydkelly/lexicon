import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';
import { decode } from 'html-entities';

if (process.argv.length !== 3) {
  console.error('Usage: node modify-dom.js path/to/filename.html');
  process.exit(1);
}
const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const partialHtml = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(partialHtml, { decodeEntities: false }, false);

$('html').remove(); // Removes <html> and its content
$('title').remove();
$('meta').remove();
$('link').remove();
$('script[src]').remove();
$('style').remove();
// $('br').remove();
$('center').remove();
$('[style]').removeAttr('style');
$('div.search1').remove(); // This should work as there's a div with id="search1"
$('div.altindex').remove(); // This is a class-based selector, not an ID
$('div.navig1').remove(); // The element uses a class, not an ID

// Replace <div class="title1"> with <h1 class="title1">
const $div = $('div.title1');
const innerContent = $div.html(); // Get the content inside the div
$div.replaceWith(`<h1
 class="bx--type-expressive-heading-01">${innerContent}</h1>`);

const $haut = $('div#haut');
const hautContent = $haut.html(); // Get the content inside the div
$haut.replaceWith(`
  <Tile class="haut">
  <Grid>
  <Row>
  <Column>
      ${hautContent}
  </Column>
  </Row>
  </Grid>
  </Tile>
`);

// Select the single div#bas element
const $divbas = $('div#bas');
const basContent = $divbas.html();

// Replace div#bas with the Tile structure
$divbas.replaceWith(`
<Tile id="bas">
  <Grid>
  <Row>
  <Column>
  <StructuredList>
    <StructuredListBody>
      ${basContent}
    </StructuredListBody>
  </StructuredList>
  </Column>
  </Row>
  </Grid>
</Tile>
`);

const $TtlP = $('p.TtlP');
const columnHtml = `
  <Row>
  <Column>
  <p class="TtlP">${$TtlP.html()}</p>
  </Column>
  </Row>
`;

const $tileBas = $('Tile#bas');
$tileBas.find('Grid').prepend(columnHtml);
$TtlP.remove();

$('a[href*=".htm"]').attr('href', (_, href) => href.replace('.htm', ''));

$('.maud iframe').each((index, iframe) => {
  const $iframe = $(iframe);
  const $container = $iframe.closest('.maud'); // Find the parent .maud container
  const smallText = $container.find('small').text().trim(); // Get the text from the <small> element
  // Set default attributes
  // $iframe.attr('height', '100%');
  $iframe.attr('width', '80%');
  $iframe.attr('aspect-ratio', '16 / 9');
  $iframe.attr('title', smallText || 'aucun titre'); // Use the extracted text or a default value
  $iframe.attr('class', 'lozad');
  $iframe.removeAttr('onload'); // Correctly remove the 'onload' attribute
});

// lazy load imgs
$('img').each((index, img) => {
  const $img = $(img);

  $img.attr('class', 'lozad');
  $img.removeAttr('onload'); // Correctly remove the 'onload' attribute
});

// controls must be true or the item will not display;  something in sveltekit
$('.maud audio').each((index, audio) => {
  const $audio = $(audio);
  $audio.attr('controls', true);
});

$('b.clnknt > a[onclick]').each((index, anchor) => {
  const $anchor = $(anchor);
  const $bWrapper = $anchor.parent('b.clnknt'); // Store reference to parent <b>
  const onclickValue = $anchor.attr('onclick');
  const anchorHtml = $anchor.html().replace(/→/g, '').trim();
  const title = $anchor.attr('title') || 'exemples / Corpus net non-tonal'; // Default title if missing
  const blueTag = $('<Tag>')
    .attr('onclick', onclickValue)
    .attr('title', title)
    .attr('type', 'blue')
    .attr('interactive', '')
    .attr('icon', '{ArrowRight}')
    .html(anchorHtml);

  if ($bWrapper.length) {
    $bWrapper.replaceWith(() =>
      $('<span>')
        .addClass('clnknt')
        .attr('data-title', title) // Store title as a data attribute
        .append(blueTag)
    );
  }
});
//
$('b.clnk > a[onclick]').each((index, anchor) => {
  const $anchor = $(anchor);
  const $bWrapper = $anchor.parent('b.clnk'); // Store reference to parent <b>
  const onclickValue = $anchor.attr('onclick');
  const TooltipTextValue = 'exemples dans Corbama-net-tonal'; // Tooltip content

  const tooltip = $('<TooltipIcon>')
    .attr('onclick', onclickValue) // Apply onclick to TooltipDefinition
    .attr('icon', '{ArrowRight}') // Apply icon to TooltipDefinition
    .attr('tooltipText', TooltipTextValue);

  $anchor.replaceWith(tooltip);

  if ($bWrapper.length) {
    $bWrapper.replaceWith(() => $('<span>').addClass('clnk').append(tooltip));
  }
});

$('a[onclick]').each((index, anchor) => {
  const $anchor = $(anchor);
  const anchorHtml = $anchor.html().replace(/→/g, '').trim();
  const onclickValue = $anchor.attr('onclick');
  const title = $anchor.attr('title') || 'exemples / Corpus net non-tonal'; // Default title if not exists
  const greenTag = $('<Tag>')
    .attr('onclick', onclickValue)
    .attr('type', 'green')
    .attr('title', title)
    .attr('interactive', '')
    .attr('icon', '{ArrowRight}')
    .html(anchorHtml);

  const wrappedTag = $('<span>')
    .addClass('clnknt')
    .attr('data-title', title) // Store title as a data attribute
    .append(greenTag);

  $anchor.replaceWith(wrappedTag); // Replace the anchor with the wrapped element
});

// these are the icons that display the reader etc for audio files
const words = ['gzd', 'gkc', 'gsc', 'gam', 'gsv'];

$('i').each((index, element) => {
  const $element = $(element);
  const classes = $element.attr('class')?.split(' ') || [];

  if (classes.some((className) => words.includes(className))) {
    const classAttr = classes.length > 0 ? ` class="${classes.join(' ')}"` : '';
    const tooltipContent = `<Tooltip${classAttr} icon="{Information}">{tooltips.content}</Tooltip>`;
    $element.replaceWith(tooltipContent);
  }
});

$('.maud small').each((index, element) => {
  const $element = $(element);
  // Remove the previous <br> if it exists
  const $prev = $element.prev();
  if ($prev.is('br')) {
    $prev.remove();
  }
  // Replace <small> with <span class="maud-text">
  $element.replaceWith(`<span class="maud-text">${$element.html()}</span>`);
});

$('img:not([alt])').attr('alt', 'unknown');

// lets remove these for now.
//  fix me.   big job as 01=a 02=b 07=f (not e; go figure)
const removedItems = [];
const logFilePath = 'removed_items.txt';
$('a').each((index, element) => {
  const $element = $(element);
  const hrefValue = $element.attr('href');
  // Check if href starts with a number
  if (hrefValue && !isNaN(parseInt(hrefValue.charAt(0)))) {
    removedItems.push($element[0].outerHTML);
    $element.remove();
  }
});
//Log the removed items to a file// not working
fs.writeFileSync(logFilePath, removedItems.join('\n'));

// Process all elements with onclick attributes for svelte syntax
$('[onclick]').each((i, elem) => {
  let onclickValue = $(elem).attr('onclick');

  // Decode HTML entities (e.g., &quot; becomes ")
  onclickValue = decode(onclickValue);
  onclickValue = onclickValue.replace(/"/g, "'");
  $(elem).attr('onclick', `{() => ${onclickValue}}`);
});

$('a[class="ve"]').each(function () {
  const classValue = $(this).attr('class');
  const nameValue = $(this).attr('name');
  const content = $(this).html();

  $(this).replaceWith(`<span id="${nameValue}" class="${classValue}">${content}</span>`);
});

// this should not have content;  its an invisible anchor
$('a[name]').each(function () {
  const nameValue = $(this).attr('name');
  $(this).replaceWith(`<span id="${nameValue}"></span>`);
});

$('p.lxP2').each((index, pElement) => {
  const $pElement = $(pElement);
  // Set language attributes for nested spans
  $pElement.find('span.Exe').attr('lang', 'bm');
  $pElement.find('span.GlFr, span.GlFr1').attr('lang', 'fr').attr('itemprop', 'description'); // Add item
});

const extractWordsAndDescriptions = () => {
  const structuredData = [];

  $('p.lxP').each((index, pElement) => {
    const $pElement = $(pElement);
    // Find the first span.Lxe inside the current p.lxP
    const $lxeSpan = $pElement.find('span.Lxe').first();

    if ($lxeSpan.length) {
      // Set the itemprop and lang attributes for the Lxe span
      // remove this for now as lang bm is set above and should not itemprop be assigned to p.lxP
      // $lxeSpan.attr('itemprop', 'name').attr('lang', 'bm');
      const lxeId = $lxeSpan.attr('id');
      // Find the next p.lxP2 and the first GlFr1 span inside it
      const $nextP = $pElement.nextAll('p.lxP2').first();
      const $nextGlFr1 = $nextP.find('span.GlFr1').first();

      if ($nextGlFr1.length && lxeId) {
        $nextGlFr1.attr('id', `${lxeId}-fr`).attr('lang', 'fr');
      }
      const word = $lxeSpan.text().trim(); // Get the word
      const description = $nextGlFr1.length ? $nextGlFr1.text().trim() : ''; // Ensure fallback for description

      structuredData.push({ word, description });
    }
  });

  return structuredData;
};

// get 5 random words for the words.json file
// Disqualifying terms (case-sensitive)
const disqualifyingTerms = ['NOM', 'CNTRL', 'AUG', 'TOP'];
// Extract all words
let wordsAndDescriptions = extractWordsAndDescriptions();
wordsAndDescriptions = wordsAndDescriptions.filter(
  ({ description }) => !disqualifyingTerms.some((term) => description.includes(term))
);

// Shuffle the words and select up to 5 random ones
const getRandomSample = (arr, num) =>
  arr.length > num ? arr.sort(() => 0.5 - Math.random()).slice(0, num) : arr;
wordsAndDescriptions = getRandomSample(wordsAndDescriptions, 5);
const jsonFilePath = path.join(path.dirname(absoluteFilePath), 'words.json');
fs.writeFileSync(jsonFilePath, JSON.stringify(wordsAndDescriptions, null, 2), 'utf-8');

// Process all elements with class attributes
$('[class]').each((_, el) => {
  const currentClass = $(el).attr('class');
  // Reassign to ensure double quotes are used
  $(el).attr('class', currentClass); // Cheerio normalizes to double quotes automatically
});
// Process all elements with title attributes
$('[title]').each((_, el) => {
  const currentTitle = $(el).attr('title');
  // Reassign to ensure double quotes are used
  $(el).attr('title', currentTitle); // Cheerio uses double quotes automatically
});
// Manually adjust the casing of <Tooltip> tags to ensure they are preserved
const modifiedHtml = $.html()
  .replace(/<tooltipdefinition/g, '<TooltipDefinition')
  .replace(/<\/tooltipdefinition>/g, '</TooltipDefinition>')
  .replace(/<tooltipicon/g, '<TooltipIcon')
  .replace(/<\/tooltipicon>/g, '</TooltipIcon>')
  .replace(/<tooltip/g, '<Tooltip')
  .replace(/<\/tooltip>/g, '</Tooltip>')
  .replace(/<tile/g, '<Tile')
  .replace(/<\/tile/g, '</Tile')
  .replace(/<tag/g, '<Tag')
  .replace(/<\/tag/g, '</Tag')
  .replace(/head=""/g, 'head')
  .replace(/interactive=""/g, 'interactive')
  .replace(/icon="\{(.*?)\}"/g, 'icon={$1}') // Fix Svelte expressions
  .replace(/<structuredlistcell/g, '<StructuredListCell')
  .replace(/<\/structuredlistcell>/g, '</StructuredListCell>')
  .replace(/<structuredlistbody/g, '<StructuredListBody')
  .replace(/<\/structuredlistbody>/g, '</StructuredListBody>')
  .replace(/<structuredlistrow/g, '<StructuredListRow')
  .replace(/<\/structuredlistrow>/g, '</StructuredListRow>')
  .replace(/<structuredlisthead/g, '<StructuredListHead')
  .replace(/<\/structuredlisthead>/g, '</StructuredListHead>')
  .replace(/<structuredlist/g, '<StructuredList')
  .replace(/<\/structuredlist>/g, '</StructuredList>');

// Save the modified HTML to the original file
// const modifiedHtml = $.html();
fs.writeFileSync(absoluteFilePath, modifiedHtml);
