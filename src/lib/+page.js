import words from './words.json';

export function load() {
  return { words };
}
