export function goct(lx, ps, gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  //window.open('http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29'+lx+'.%2A"+%26+lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]','Corpus tonal')
  lx = lx.replace('|', '%7C');
  window.open(
    'http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&tab=advanced&queryselector=cql&attrs=word%2Ctag%2Cgloss&viewmode=kwic&attr_allpos=kw&shorten_refs=1&glue=1&gdexcnt=300&itemsPerPage=20&structs=s%2Cg&refs=%3Ddoc.id%2C%23&default_attr=lemma&showresults=1&f_tab=basic&f_showrelfrq=1&operations=%5B%7B%22name%22%3A%22cql%22%2C%22arg%22%3A%22%5Bword%3D%5C%22(%3Fi)' +
      lx +
      '.*%5C%22%20%26%20lemma%3D%5C%22' +
      lx +
      '%5C%22%20%26%20tag%3D%5C%22' +
      ps +
      '%5C%22%20%26%20gloss%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22query%22%3A%7B%22queryselector%22%3A%22cqlrow%22%2C%22cql%22%3A%22%5Bword%3D%5C%22(%3Fi)' +
      lx +
      '.*%5C%22%20%26%20lemma%3D%5C%22' +
      lx +
      '%5C%22%20%26%20tag%3D%5C%22' +
      ps +
      '%5C%22%20%26%20gloss%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22default_attr%22%3A%22lemma%22%7D%7D%5D',
    'Corpus net tonal'
  );
}
export function gocnt(lx, ps, gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  // window.open('http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29'+lx+'.%2A"+%26+lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]','Corpus non tonal')
  lx = lx.replace('|', '%7C');
  window.open(
    'http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&tab=advanced&queryselector=cql&attrs=word%2Ctag%2Cgloss&viewmode=kwic&attr_allpos=kw&shorten_refs=1&glue=1&gdexcnt=300&itemsPerPage=20&structs=s%2Cg&refs=%3Ddoc.id%2C%23&default_attr=lemma&showresults=1&f_tab=basic&f_showrelfrq=1&operations=%5B%7B%22name%22%3A%22cql%22%2C%22arg%22%3A%22%5Bword%3D%5C%22(%3Fi)' +
      lx +
      '.*%5C%22%20%26%20lemma%3D%5C%22' +
      lx +
      '%5C%22%20%26%20tag%3D%5C%22' +
      ps +
      '%5C%22%20%26%20gloss%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22query%22%3A%7B%22queryselector%22%3A%22cqlrow%22%2C%22cql%22%3A%22%5Bword%3D%5C%22(%3Fi)' +
      lx +
      '.*%5C%22%20%26%20lemma%3D%5C%22' +
      lx +
      '%5C%22%20%26%20tag%3D%5C%22' +
      ps +
      '%5C%22%20%26%20gloss%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22default_attr%22%3A%22lemma%22%7D%7D%5D',
    'Corpus net non tonal'
  );
}
export function goct_mrph(gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  //window.open('http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[tag%3D"'+gloss+'"]','Corpus tonal')
  window.open(
    'http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-tonal&tab=advanced&queryselector=cql&attrs=word&viewmode=kwic&attr_allpos=all&shorten_refs=1&glue=1&gdexcnt=300&itemsPerPage=20&structs=s%2Cg&refs=%3Ddoc.id&default_attr=lemma&showresults=1&f_tab=basic&f_showrelfrq=1&operations=%5B%7B%22name%22%3A%22cql%22%2C%22arg%22%3A%22%5Btag%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22query%22%3A%7B%22queryselector%22%3A%22cqlrow%22%2C%22cql%22%3A%22%5Btag%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22default_attr%22%3A%22lemma%22%7D%7D%5D',
    'Corpus net tonal'
  );
}
export function gocnt_mrph(gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  //window.open('http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[tag%3D"'+gloss+'"]','Corpus non tonal')
  window.open(
    'http://cormande.huma-num.fr/corbama/#concordance?corpname=corbama-net-non-tonal&tab=advanced&queryselector=cql&attrs=word&viewmode=kwic&attr_allpos=all&shorten_refs=1&glue=1&gdexcnt=300&itemsPerPage=20&structs=s%2Cg&refs=%3Ddoc.id&default_attr=lemma&showresults=1&f_tab=basic&f_showrelfrq=1&operations=%5B%7B%22name%22%3A%22cql%22%2C%22arg%22%3A%22%5Btag%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22query%22%3A%7B%22queryselector%22%3A%22cqlrow%22%2C%22cql%22%3A%22%5Btag%3D%5C%22' +
      gloss +
      '%5C%22%5D%22%2C%22default_attr%22%3A%22lemma%22%7D%7D%5D',
    'Corpus net non tonal'
  );
}
