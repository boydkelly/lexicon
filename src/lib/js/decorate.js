/**
 * Decorate SEO title.
 * @param {string} title
 */
export default function (title) {
	return `${title} – Boyd Kelly`;
}
