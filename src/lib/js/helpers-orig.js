export function goct(lx, ps, gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  window.open(
    'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29' +
      lx +
      '.%2A"+%26+lemma%3D"' +
      lx +
      '"+%26+tag%3D"' +
      ps +
      '"+%26+gloss%3D"' +
      gloss +
      '"]',
    'Corpus tonal'
  );
}

export function gocnt(lx, ps, gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  window.open(
    'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[word%3D"%28%3Fi%29' +
      lx +
      '.%2A"+%26+lemma%3D"' +
      lx +
      '"+%26+tag%3D"' +
      ps +
      '"+%26+gloss%3D"' +
      gloss +
      '"]',
    'Corpus non tonal'
  );
}

export function goct_mrph(gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  window.open(
    'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-tonal&queryselector=cqlrow&cql=[tag%3D"' +
      gloss +
      '"]',
    'Corpus tonal'
  );
}

export function gocnt_mrph(gloss) {
  //parent.location='http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[lemma%3D"'+lx+'"+%26+tag%3D"'+ps+'"+%26+gloss%3D"'+gloss+'"]'
  window.open(
    'http://cormande.huma-num.fr/corbama/run.cgi/first?corpname=corbama-net-non-tonal&queryselector=cqlrow&cql=[tag%3D"' +
      gloss +
      '"]',
    'Corpus non tonal'
  );
}
