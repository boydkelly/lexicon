// src/lib/stores/disamb.js
import { writable } from 'svelte/store';

// Initialize the store with the current value from localStorage or default to false
// export const isDisamb = writable(localStorage.getItem('isDisamb') === 'true');
export const isDisamb = writable(false);
// Sync the store with localStorage
// isDisamb.subscribe((value) => {
//   localStorage.setItem('isDisamb', value);
// });
