import fs from 'fs';
import path from 'path';
import * as cheerio from 'cheerio';

if (process.argv.length !== 3) {
  console.error('Usage: node script.js path/to/filename.html');
  process.exit(1);
}

const [, , relativeFilePath] = process.argv;
const absoluteFilePath = path.resolve(relativeFilePath);
const html = fs.readFileSync(relativeFilePath, 'utf-8');
const $ = cheerio.load(html, { decodeEntities: false });

const lxPElements = $('p.lxP');

lxPElements.each((index, elem) => {
  if (index !== 2) return; // Skip all except the 3rd (0-based index)

  console.log(`Iteration ${index + 1}: Processing <p class="lxP">`);
  const siblings = $(elem).nextUntil('p.lxP');

  console.log(`Siblings before next <p class="lxP">:`);
  siblings.each((i, sibling) => {
    console.log($.html(sibling));
  });
});
