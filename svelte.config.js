// svelte.config.js
import adapter from '@sveltejs/adapter-static';
import { optimizeImports } from 'carbon-preprocess-svelte';

const config = {
  kit: {
    prerender: {
      handleHttpError: 'ignore',
      handleMissingId: 'ignore'
    },
    adapter: adapter({
      // default options are shown. On some platforms
      // these options are set automatically — see below
      pages: 'build',
      assets: 'build',
      fallback: null,
      precompress: false,
      strict: false,
      prerender: {
        default: true
      }
    })
  },
  extensions: ['.svelte', '.htm'],
  //remember to checkout the the carbon settings for vite.  only in prooduction.
  preprocess: [[optimizeImports()]]
};

export default config;
