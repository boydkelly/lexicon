#!/usr/bin/bash
#npx gulp copyLexicon
[ -d "src/routes/lexicon" ] || exit

dest="src/routes/lexicon"

while IFS= read -r -d '' file; do
  echo "$file"

  # the following must run before prettier otherwise prettier will choke over </p>.
  sed -i 's/\(<div id="bas">\)<\/p>/\1/g' "$file"

  #br standardize element for html5
  sed -i 's/<br\s*\/>/<br>/g' "$file"

  #unnecessary %nbsp
  sed -i -e 's/^&nbsp;//g' "$file"

  #remove all extra line breaks breaks a lot don't do...
  #sed -r -i 's/(\s*)(.*<br *\/>)(.*)/\3/' "$file"
  # ok lets remove only those on a single line
  #  sed -i '/^<br>$/d' "$file"

  #replace tabs with 4 spaces
  sed -i 's/\t/    /g' "$file"

  #npx prettier --svelte-strict-mode -w "$file"

  #remove all blank lines
  sed -i '/^$/d' "$file"

  #remove trailing space
  sed -i 's/[[:space:]]*$//' "$file"

  #réduire les espaces entre les elements
  sed -i 's/ \{2,\}/ /g' "$file"

  # convertir les href guillements simple en double
  sed -i 's/href='\''\([^'\'']*\)'\''/href="\1"/g' "$file"

  #just for clarity make a line return after the initial headword and variations
  sed -i 's/<\/a><p/<\/a>\n<p/' "$file"
  #remove double spaces
  #sed -i '/^$/N;/\n$/D' "$file"

  # end all </span>s (not already at eol) with a new line for easier manipulation with sed
  sed -i 's/<\/span>/<\/span>\n/g' "$file"

  #add line break after </p> <br>
  sed -i 's/<\/p>/<\/p>\n/g' "$file"
  sed -i 's/<br>/<br>\n/g' "$file"

  #insert linebreak at bold element
  sed -i 's/><b/>\n<b/' "$file"
  sed -i 's/><span/>\n<span/' "$file"

  #=====  fixes below

  #adding missing </span> class that occurs often within Mnhva/Mnhvam class and perhaps elsewhere
  #	sed -i -r 's/<\/a>\.  <span/<\/a>\.<\/span><span/' "$file"
  #	sed -i 's/\(<span class="Mnhva"\)\(.*\)\(<span.*\)/\1\2<\/span>\3/p' "$file"
  #	sed -i 's/\(<span class="Mnhva\)\(.*<\/a>\)\(\. *<span.*\)/\1\2<\/span>\3/' "$file"
  #target Ala Aliman and many others missing </span>;   Add </span but not where Etym is found
  sed -i '/Etym\|Source\|Voir/! s/\(<span class="Mnhva\)\(.*<\/a>\)\(\. *<span.*\)/\1\2<\/span>\3/' "$file"

  #target arrondissment and others that have extra </span>;  add <span>
  sed -i 's/^ *<a href.*<\/a>.<\/span>$/<span>&/p' "$file"

  # handle the "</b>"
  # Ce n'est pas tout à fait claire si c'est une fermature de bold ou bien retour à la ligne
  # lets start by nuking it.  easy but maybe better after to rather add a <br>
  # make the </b> at the eol in a few cases that would be missed by next command.
  sed -i 's/<\/a><\/b><a/<\/a><\/b>\n<a/g' "$file"
  sed -i 's/\([[:space:]]title="exemples\)\(.*\)\(<\/b>$\)/\1\2\n/' "$file"

  # value= avec guillements
  sed -i 's/Siby, 2012<\/i>/<i>&/' "$file"
  sed -i 's/<\/i>Hymne du Mali/<i>Hymne du Mali/' "$file"
  sed -i 's/<maninka/maninka/' "$file"

  sed -i 's/"Monsieur"/'\''Monsieur'\''/' "$file"
  sed -i 's/"ntomo"/'\''ntomo'\''/' "$file"
  sed -i 's/"biiii"/'\''biiii'\''/' "$file"
  sed -i 's/"choser"/'\''choser'\''/' "$file"
  sed -i 's/"korè"/'\''korè'\''/' "$file"
  #fix bad quotes
  #replace all quotes with spaces; then restore for name= and class= then for closing chevron
  #this could be flaky;
  #not sure if this can be adapted to the lexicon side.
  #too dangerous for the value issues.   For now will solve one by one

  #	sed -i 's/value=/\n&/' "$file"
  #sed -i '/value=/ s/"/ /g; s/value= /value="/g; s/ >/">/g; s/= "/="/g' "$file"
  #version 3.   adding cr to value breaks later.   just do it right for the whole line
  #sed -i '/input/ s/"/%/g; s/=%/="/g; s/%>/">/g; s/%mmc%/"mmc"/g; s/%TEXT%/"TEXT"/g; s/:%/:/g; s/" \[/ \[/gp' "$file"

  sed -i '/input/ s/"/ /g; s/ >/">/g; s/\smmc\s/"mmc" /g; s/ TEXT  /"TEXT" /g; s/= /="/g; s/)  /)" /g; s/  / /g' "$file"

  #corrections au pif
  # iframe k.htm kibaruya
  sed -i 's/281\/0'\''"/281\/0'\''>/' "$file"

  #{} non accepté par svelte (deux endroits) (interprété comme code)
  sed -i 's/{/\⟨/g; s/}/\⟩/' "$file"
  # (< français) non accepté par svelte (interprété comme code)
  sed -i 's/< français/⟨ français/g' "$file"
  sed -i 's/< arabe/\⟨ arabe/g' "$file"
  # non accepté par svelte?  :  et sed a des difficultés avec nbs unicode. ou bien c'est moi...
  #sed -i 's/marginales.*:/marginales/' "$file"
  # another thing recongnized as code
  #sed -i 's/-\+\&gt;/——⟩/g' "$file"
  sed -i 's/-\+\&gt;/——⟩/g' "$file"
  #sed -i 's/sigi`-n-fɛ|sìgi`-ń-fɛ̀/sigi-n-fɛ/g' "$file"
  sed -i 's/i`-/i-/g' "$file"
  #sed -i 's/nsǒn`-/nsǒn-/g' "$file"
  #sed -i 's/nson`-/nson-/g' "$file"
  #this should eventually negate the above
  sed -i 's/n`-/n-/g' "$file"

  #replace ... with elipsis
  sed -i 's/\.\.\./…/' "$file"

  sed -i 's/http:/https:/' "$file"

  # deletes leading blank lines
  sed -i '/./,$!d' "$file"

  if ! head -n 1 "$file" | grep -q "<script>"; then
    sed -i '1i<script>\n</script>\n' "$file"
  fi

  sed -i "2i import { StructuredList, StructuredListHead, StructuredListRow, StructuredListCell, StructuredListBody  } from 'carbon-components-svelte';" "$file"

  found_words=()
  words=("goct" "gocnt" "goct_mrph" "gocnt_mrph")

  for word in "${words[@]}"; do
    if rg -q "$word" "$file"; then
      found_words+=("$word")
    fi
  done

  if [ ${#found_words[@]} -gt 0 ]; then
    import_statement="import { $(
      IFS=$', '
      echo "${found_words[*]}"
    ) } from \"\$lib/js/helpers.js\";"

    sed -i "2i$import_statement" "$file" #echo "Found words: ${found_words[@]}"
    sed -i "2i import ArrowRight from 'carbon-icons-svelte/lib/ArrowRight.svelte';" "$file"
  fi

  # Add this line only if "goct" is in found_words
  if [[ " ${found_words[*]} " =~ " goct " ]]; then
    sed -i "2i import { TooltipIcon } from 'carbon-components-svelte';" "$file"
  fi

  words=('gzd' 'gkc' 'gsc' 'gam' 'gsv')
  for word in "${words[@]}"; do
    if rg -q "$word" "$file"; then
      echo "$word" >>words.txt
      sed -i "2i import { Tooltip,TooltipDefinition,Tile,Tag } from 'carbon-components-svelte';" "$file"
      sed -i "2i import Information from 'carbon-icons-svelte/lib/Information.svelte';" "$file"
      sed -i '3i import { tooltips } from "$lib/js/tooltips.js";' "$file"
      break
    fi
  done

  node ./dom-semantic.js "$file"
  node ./dom-lexicon.js "$file"
  sed -i '/^[[:space:]]*$/d' "$file"
  npx prettier -w "$file"

  #npx eslint --fix "$file" && git commit "$file" -m "Eslint fixed "$file" on $iso_date" --quiet &
  #run this again after eslint
  #sed -i -e '/<\/html>/d' "$file"
  #sed -i -e '/DOCTYPE/d' "$file"

  #git diff "$file"
  #echo "$file" 3

done < <(find $dest -mindepth 2 -name "+page.svelte" -print0 | sort -z)
