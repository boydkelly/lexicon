#!/usr/bin/bash
#npx gulp copyHome
[ -d "src/routes/lexicon" ] || exit

dest="src/routes/lexicon"
# cp -v ./Bambara/lexicon/*.htm $dest
#rm -v $dest/index.htm
#git commit -a -m "Commit all files on $iso_date"
#--quiet

while IFS= read -r -d '' file; do
  tmpfile=$(mktemp)
  echo "$file"
  #dos2unix -q "$file"
  #npx prettier -w "$file"
  # Apply sed commands on the original file
  #sleep 1
  # this needs to be done befor mod-dom
  #sed -i 's|<small>(lien)</small> ||' "$file"
  #kkksleep 1
  # Copy the file to a temporary file for Node.js processing
  cp "$file" "$tmpfile"

  sed -i -r 's/(<span class="Lxe">)([^>]*)(<\/span>\s*:)/\n<h2>\n\2 :\n<\/h2>\n/' "$tmpfile"
  sed -i '/^[[:space:]]*$/d' "$tmpfile"

  #sed -i 's/<span class="Lxe">Erreurs<\/span>, /\n<h2>Erreurs :\n<\/h2>\n/' $tmpfile

  # add a </section> before each new <h2> except delete the first
  sed -i 's/<h2>/<\/section>\n<h2>/' "$tmpfile"
  sed -i '0,/<h2>/s/<\/section>//' "$tmpfile"

  # add a <section> after every </h2>
  sed -i '/<\/h2>/a<section>' "$tmpfile"

  # manual fix for last paragraph with no delimiter
  sed -i '/Claviers/a<\/section>' "$tmpfile"
  sed -i '/^[[:space:]]*$/d' "$tmpfile"
  sed -i '/^<br>$/d' "$tmpfile"
  sed -i '/^<br><br>$/d' "$tmpfile"
  #tac $tmpfile | sed '0,/<p>/s/<p>//' | tac >$file
  #  mv "$tmpfile" "$file" && exit
  #  sed -n '/<\/h2>/,/<h2>/p' $tmpfile
  # cat $tmpfile
  # Run the Node.js script on the temporary file
  mv "$tmpfile" "$file"
  tidy -config .tidyrc -m "$file"
  node ./dom-home.js "$file"
  # Copy the modified temporary file back to the original file
  #mv "$tmpfile" "$file"
  sed -i '/^[[:space:]]*$/d' "$file"
  sed -i 's/Bambaradu/Bambara du/' "$file"
  #sed -i -e 's|</h2> *:|:</h2>|g' -e 's|</h2>|&\n|g' "$file"

  # sed -i '1i <style lang="postcss"> Grid:first-child { justify-content: center; } </style>' "$file"

  # Add the script block after Node.js processes the file
  sed -i '1i<script>\nimport { Tile,Tag } from '\''carbon-components-svelte'\'';\n</script>\n' "$file"
  sed -i "2i import { StructuredList, StructuredListHead, StructuredListRow, StructuredListCell, StructuredListBody  } from 'carbon-components-svelte';" "$file"
  sed -i "2i import { Grid,Row,Column,UnorderedList,ListItem  } from 'carbon-components-svelte';" "$file"

done < <(find "$dest" -maxdepth 1 -name "Lexicon.svelte" -print0 | sort -z)
